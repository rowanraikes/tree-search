import numpy as np

start_state = np.asarray([[2,8,3],[1,6,4],[7,' ',5]])
goal_state = np.asarray([[1,2,3],[8,' ',4],[7,6,5]])

class Node():

	def __init__(self, parent, state, goal_state, g):

		self.parent = parent
		self.state = state
		self.g = g
		self.goal = goal_state
		self.h = heuristic_two(state, goal_state)

		self.f = self.g + self.h

def compare_states(state_one, state_two):
	# compare two states, return true if they are identical

	for i in range(3):
		for j in range(3):
			if state_one[i,j] != state_two[i,j]:
				return False

	return True


def find_new_states(parent_state):
	# function calculates all possible new states, returns the costs for each

	current_state = parent_state.state
	states = []

	for i in range(3):
		for j in range(3):
			if current_state[i][j] == ' ':
				blank = [i, j]
				break

	poss_moves = np.asarray([np.add(blank, [1,0]), np.add(blank, [-1,0]), np.add(blank, [0,1]), np.add(blank, [0,-1])])
	allowed_moves = []

	# find index of all squares that can be moved
	for i in range(4):
		if (poss_moves[i,0] > -1) and (poss_moves[i,1] > -1) and (poss_moves[i,0] < 3) and (poss_moves[i,1] < 3):
			allowed_moves.append(poss_moves[i])

	# calculate heuristic for each move
	for j in range(len(allowed_moves)):


		move = allowed_moves[j]

		# compute new state
		new_state = np.copy(current_state)
		new_state[blank[0]][blank[1]] = current_state[move[0]][move[1]]
		new_state[move[0]][move[1]] = ' '

		states.append(Node(parent_state, new_state, parent_state.goal, parent_state.g+1))


	return states

def heuristic_two(state, goal_state):
	# sum of manhatten distance of each misplaced tile 

	h = 0

	# loop over each digit in current state
	for i in range(3):
		for j in range(3):
			
			digit = state[i,j]

			# find digit in goal state
			goal_location = np.where(digit == goal_state)

			h += (abs(i - goal_location[0]) + abs(j - goal_location[1]))

	return h


def heuristic_one(state, goal_state):
	# number of misplaced tiles

	h = 0

	for i in range(3):
		for j in range(3):
			if state[i,j] != goal_state[i,j]:
				h += 1

	return h

def reconstruct_path(end_node):

	node = end_node
	path = [node.state]

	while node.parent != None:
		node = node.parent
		path.append(node.state)

	path.reverse()

	print('Solution:')

	for i in range(len(path)):
		print(path[i], '\n')


# Main
open_list = []
closed_list = []

start_node = Node(None, start_state, goal_state, 0)
open_list.append(start_node)

while len(open_list) > 0:

	# select node with smallest f to expand
	current_node = open_list[0]
	current_index = 0
	for i in range(len(open_list)):
		if open_list[i].f < current_node.f:
			current_node = open_list[i]
			current_index = i

	# expand selected node
	new_nodes = find_new_states(current_node)
	open_list.extend(new_nodes)
	
	# remove expanded node from open list
	open_list.pop(current_index)
	closed_list.append(current_node)

	# print current state
	nodes_generated = len(open_list) + len(closed_list)
	print(current_node.state, '\n')

	# check if goal
	if compare_states(current_node.state, current_node.goal):
		print("Solution found!")
		reconstruct_path(current_node)
		break










	

