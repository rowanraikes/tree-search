
Tree traversal problem that implements the A* algorithm to find the solution to a simple puzzle. This 
algorithm always finds the optimal solution, which in this case is the shortest sequence of moves 
required to reach the goal state.

The code prints out all the states, i.e. board configurations that it visits during the tree traversal
until the solution state is found. The optimal sequence of moves required is then printed out. 